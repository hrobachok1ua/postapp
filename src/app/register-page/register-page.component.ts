import { Component, OnInit } from '@angular/core';
import { PostsAndUsersService } from '../posts-and-users.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  signUp = true;

  constructor(private auth: PostsAndUsersService) { }

  ngOnInit() {
  }

  toggle() {
    this.signUp = !this.signUp;
  }

  register(v) {
    if (this.signUp) {
      this.auth.signUpUser(v.email, v.password);
    } else {
      this.auth.signInUser(v.email, v.password);
    }
  }

}
