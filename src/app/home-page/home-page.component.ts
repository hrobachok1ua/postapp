import { Component, OnInit, DoCheck } from '@angular/core';
import { PostsAndUsersService } from '../posts-and-users.service';
import { trigger, style, transition, animate, query, stagger } from '@angular/animations';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [
    trigger('postsStagger', [
      transition('* <=> *', [
        query(
          ':enter',
          [
            style({ opacity: 0, transform: 'translateY(-15px)' }),
            stagger(
              '50ms',
              animate(
                '550ms ease-out',
                style({ opacity: 1, transform: 'translateY(0px)' })
              )
            )
          ],
          { optional: true }
        ),
        query(':leave', animate('50ms', style({ opacity: 0 })), {
          optional: true
        })
      ])
    ])
  ]
})
export class HomePageComponent implements OnInit, DoCheck {

  posts;
  counter = 5;
  showDeleted = false;
  deletedArr = [];

  constructor(private postsAndUsersService: PostsAndUsersService) {  }

  restrictShowDeleted(posts) {
    const d = this.postsAndUsersService.deletedArr;
    const p = posts;
    for (let dEl in d) {
      for (let el in p) {
        if (p[el].key === d[dEl].key) {
          p.splice(+el, 1);
        }
      }
    }
    return p;
  }

  hideDeletedBin() {
    this.showDeleted = false;
  }

  showDeletedBin() {
    this.showDeleted = true;
  }

  showMore() {
    console.log(this.posts.length);
    this.counter += 5;
    this.loadPosts();
    console.log(this.posts.length);
  }

  sortAsc(a, b) {
    if (a.date > b.date) {
      return 1;
    }
    if (a.date < b.date) {
      return -1;
    }
    return 0;
  }

  loadPosts() {
    if (this.postsAndUsersService.admin) {
      this.postsAndUsersService.getPosts()
      .subscribe(post => {
          this.posts = this.restrictShowDeleted(post.sort(this.sortAsc).slice(0, this.counter));
        });
      } else {
      this.postsAndUsersService.getPostsOnlySend()
      .subscribe(post => {
        this.posts = this.restrictShowDeleted(post.sort(this.sortAsc).slice(0, this.counter));
     });
    }
  }

  ngDoCheck() {
    if (this.postsAndUsersService.shouldRender) {
      this.postsAndUsersService.shouldRender = false;
      this.loadPosts();
    }

    this.deletedArr = this.postsAndUsersService.deletedArr;
  }

  ngOnInit() {
    this.loadPosts();
  }

  onMethod(el) {
    if (el.value.includes('Ascending')) {
      this.loadPosts();
    } else if (el.value.includes('Descending')) {
      if (this.postsAndUsersService.admin) {
        this.postsAndUsersService.getPosts()
        .subscribe(post => {
          this.posts = this.restrictShowDeleted(post.sort((a, b) => {
            if (a.date < b.date) {
              return 1;
            }
            if (a.date > b.date) {
              return -1;
            }
            return 0;
          }).slice(0, this.counter));
        });
      } else {
        this.postsAndUsersService.getPostsOnlySend()
        .subscribe(post => {
          this.posts = this.restrictShowDeleted(post.sort((a, b) => {
            if (a.date < b.date) {
              return 1;
            }
            if (a.date > b.date) {
              return -1;
            }
            return 0;
          }).slice(0, this.counter));
       });
      }
    }
  }

  onType(el) {
    if (this.postsAndUsersService.admin) {
      this.postsAndUsersService.getPosts()
      .subscribe(post => {
        const p = post.sort(this.sortAsc);
        this.posts = this.restrictShowDeleted(p.filter(e => e.type === el.value).slice(0, this.counter));
      });
    } else {
      this.postsAndUsersService.getPostsOnlySend()
      .subscribe(post => {
        const p = post.sort(this.sortAsc);
        this.posts = this.restrictShowDeleted(p.filter(e => e.type === el.value).slice(0, this.counter));
     });
    }
  }

  onAuthor(el) {
    if (el.value === '') {
      this.loadPosts();
    } else {
      if (this.postsAndUsersService.admin) {
        this.postsAndUsersService.getPosts()
        .subscribe(post => {
          const p = post.sort(this.sortAsc);
          this.posts = this.restrictShowDeleted(p.filter(e => e.author.includes(el.value)).slice(0, this.counter));
        });
      } else {
        this.postsAndUsersService.getPostsOnlySend()
        .subscribe(post => {
          const p = post.sort(this.sortAsc);
          this.posts = this.restrictShowDeleted(p.filter(e => e.author.toLowerCase().includes(el.value)).slice(0, this.counter));
       });
      }
    }
  }

  onSearch(el) {
    if (el.value === '') {
      this.loadPosts();
    } else {
      if (this.postsAndUsersService.admin) {
        this.postsAndUsersService.getPosts()
        .subscribe(post => {
          const p = post.sort(this.sortAsc);
          this.posts = this.restrictShowDeleted(p.filter(e => (e.title.includes(el.value) || e.description.includes(el.value)))
          .slice(0, this.counter));
        });
      } else {
        this.postsAndUsersService.getPostsOnlySend()
        .subscribe(post => {
          const p = post.sort(this.sortAsc);
          this.posts = this.restrictShowDeleted(p.filter(e => (e.title.includes(el.value) || e.description.includes(el.value)))
          .slice(0, this.counter));
       });
      }
    }
  }

}
